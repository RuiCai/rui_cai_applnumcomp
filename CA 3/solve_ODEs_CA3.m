%%
% Problem:
%
% gas-phase reactions occur in a PFR: 
%    Reaction1:   A -> B       -r1A = k1A*CA
%    Reaction2:   2A -> C      -r2A = k2A*CA^2
% 
% Purpose & output:
%
%    Figure 1 The relationship between temperature and system volume
%    Figure 2 The relationship between A,B and C molar how rates and system volume
%
% No input, the system parameters can be changed in ODEs_CA3.m.
% 
% The units and definitions of parameters:
%
%    HRx1A = -20000 J/(mol of A reacted in reaction 1) 
%    HRx2A = -60000 J/(mol of A reacted in reaction 2)
%    CpA = 90 J/mol��C
%    CpB = 90 J/mol��C
%    CpC = 180 J/mol��C
%    Ua = 4000 J/m^3�s��C
%    Ta = 373 K (Constant) 
% at the beginning, Pure A is fed at a rate of 100 mol/s, a temperature of 150�C, and a concentration of 0.1 mol/dm^3
%    FA = 100 mol/s
%    FB = 0 mol/s
%    FC = 0 mol/s
%    T0 = 423 K
%    CT0 = 0.1;
%
% Author:
%
%     Rui Cai
%%
function solve_ODEs_CA3

[x,y]=ode45(@ODEs_CA3,[0:0.01:1],[100,0,0,423]);

% plot
figure(1)
plot(x,y(:,4))
xlabel('V(dm^3)')
ylabel('T(K)')
legend('T','Location','Best');
title('Temperature profile.')

figure(2)
plot(x,y(:,1),'r--')
hold on 
plot(x,y(:,2),'bs')
plot(x,y(:,3),'g:*')
hold off
xlabel('V(dm^3)')
ylabel('F(mol/s)')
legend('FA','FB','FC','Location','Best');
title('Profile of molar flow rates FA, FB, and FC.')









