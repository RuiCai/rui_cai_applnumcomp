function dV = ODEs_CA3(V,F_and_T)
%value of parameters
HRx1A = -20000; 
HRx2A = -60000;
CpA = 90;
CpB = 90;
CpC = 180;
Ua = 4000;
Ta = 373;
FA = F_and_T(1);
FB = F_and_T(2);
FC = F_and_T(3);
T = F_and_T(4);
FT = FA + FB + FC;    

T0 = 423;
CT0 = 0.1;
CA = CT0*(FA/FT)*(T0/T);
CB = CT0*(FB/FT)*(T0/T);
CC = CT0*(FC/FT)*(T0/T);

E1overR = 4000;
k1A = 10*exp(E1overR*(1/300-1/T));
E2overR = 9000;
k2A = 0.09*exp(E2overR*(1/300-1/T)); 

r1A = -k1A*CA;
r2A = -k2A*CA^2;
rB = k1A*CA;
rC = 0.5*k2A*CA^2;

dV =zeros(4,1);
dV(1) = r1A+r2A;
%dFA/dV
dV(2) = rB;
%dFB/dV
dV(3) = rC;
%dFC/dV
dV(4) = (Ua*(Ta-T) + (-r1A)*(-HRx1A) + (-r2A)*(-HRx2A))/(FA*CpA + FB*CpB + FC*CpC);
%dT/dV
end