function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 13-Dec-2019 04:06:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
% input_diffeqtns=get(handles.edit3,'String')
% input_diffeqtns=str2double(input_diffeqtns);
% save('input_diffeqtns')
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=eqtns;
Neqtns=get(handles.edit1, 'String')
Neqtns=str2num(Neqtns);
save('Neqtns.mat','Neqtns');
for i=1:Neqtns
    run('eqtns')
    uiwait
    load('diffeqtns.mat')
    odes{i}=strcat('d',numer,'/d',denom,'=',diffeqtn)
handles.diffeqt{i}=diffeqtn
handles.num{i}=numer
handles.den{i}=denom
handles.IC(i)=str2num(ICvalue)
guidata(hObject,handles)
end
set(handles.ODEs,'String',odes)



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=explicit_eqtns;
Nexplicit=get(handles.edit2, 'String')
Nexplicit=str2num(Nexplicit);
save('Nexplicit.mat','Nexplicit');

if Nexplicit==0
    handles.Nexp={};
else
    for i=1:Nexplicit
        run('explicit_eqtns')
        uiwait
        load('explicit_eqtns.mat','expliciteqtn')
        explicit{i}=expliciteqtn;
        handles.Nexp{i}=expliciteqtn;
        guidata(hObject,handles)
    end
set(handles.Explicit_equations,'String',explicit)
end


% --- Executes on button press in Solve.
function Solve_Callback(hObject, eventdata, handles)
% hObject    handle to Solve (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tinit=get(handles.Tinit,'String');
to=str2num(tinit)
tfinal=get(handles.Tfinal,'String');
tfinal=str2num(tfinal)
% load('explicit_eqtns.mat','expliciteqtn')

[t,y]=ode45(@(t,y)ODE(t,y,handles.diffeqt,handles.num,handles.den,handles.Nexp),[to tfinal],handles.IC);
t;
y;

handles.t=t;
handles.y=y;
guidata(hObject,handles)


axes(handles.plots);

load('Neqtns.mat','Neqtns')
neqn=Neqtns
for i=1:neqn
p(i)=plot(t,y(:,i))
hold on
legend_labels{i} = sprintf('%s', handles.num{i})
legend(p,legend_labels{:})
xlabel(handles.den(1))
ylabel('y')
end 
hold off

% 
% ax=handles.plots
% figure_handle=isolate_axes(ax)
% export_fig plots.png



% fr = getframe(handles.plots);
% imwrite(fr.cdata, 'plots.jpg')



function dydt = ODE(t, y,diffeqt,num,den,Nexp)

load('explicit_eqtns.mat','expliciteqtn')

 % Input: t and y are the independent and dependent variable values
 % denominators, numerators, RHSs, and explicitEqns are cell
 % arrays with the first three terms defining the ODEs of the form
 % d(numerators) / d(denominators) = RHSs and the fourth term
 % defining the associated explicit equations
 % Output: the derivative vector dy/dt for y(1):y(numODEs) where
 % numODEs = length(numerators) = length(RHSs) = length(denominators)

% independent variable
str0=cell2mat(den(1));
eval(strcat(str0,'= t;'));

 % dependent variables
for i = 1:length(num)
str1 = cell2mat(num(i));
eval(strcat(str1,'= y(i);'));
end
% explicit equations provided in MATLAB acceptable order, ...
% semicolon separated list
for i = 1:length(Nexp)
str2 = cell2mat(Nexp(i));
eval([str2,';']);
end  
 % Right-hand sides of ODE definitions
for i = 1:length(diffeqt)
str3 = cell2mat(diffeqt(i));
dydt(i) = eval(str3);
end

dydt = dydt';



function Tinit_Callback(hObject, eventdata, handles)
% hObject    handle to Tinit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tinit as text
%        str2double(get(hObject,'String')) returns contents of Tinit as a double
% global tinit
% tinit=get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function Tinit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tinit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tfinal_Callback(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tfinal as text
%        str2double(get(hObject,'String')) returns contents of Tfinal as a double
% global tf
% tf=get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function Tfinal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function plots_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate plots


% --- Executes on button press in pushbutton4.


% --- Executes on button press in Save_plot.
function Save_plot_Callback(hObject, eventdata, handles)
% hObject    handle to Save_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% fh=figure;
% copyobj(handles.plots,fh);
% saveas(fh,'Plot_result','jpg');
[file,path,indx] = uiputfile('Plot.png');
ax = handles.plots;
figure_handle = isolate_axes(ax);
export_fig Plot.png



% --- Executes during object creation, after setting all properties.
function ODEs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODEs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Explicit_equations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Explicit_equations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Exportxls.
function Exportxls_Callback(hObject, eventdata, handles)
% hObject    handle to Exportxls (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
[file,path,indx] = uiputfile('result.xls');
xlswrite('result.xls',handles.den,'Sheet 1','A1')
xlswrite('result.xls',handles.num,'Sheet 1','B1')

xlswrite('result.xls',handles.t,'Sheet 1','A2')
xlswrite('result.xls',handles.y,'Sheet 1','B2')

% handles    structure with handles and user data (see GUIDATA)
