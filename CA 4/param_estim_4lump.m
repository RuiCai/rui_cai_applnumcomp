%% Problem&Purpose
% 
%The four-lump model involves four subgroups: VGO (y1), gasoline (y2), gas
%(y3), and coke (y4). The equations that describe the four-lump 
%model are:
%
%    dy1/dt = - (k12 + k13 +k14)*y1^2
%    dy2/dt = k12*y1^2 - k23*y2 - k24*y2
%    dy3/dt = k13*y1^2 + k23*y2
%    dy3/dt = k14*y1^2 + k24*y2 
%
%   Figure 1 The relationship between time and components fraction
%   Figure 2 The relationship between convension value and components
%    fraction
% 
% The units and definitions of parameters:
%
% Time (h)
% y1: VGO weight fraction
% y2: gasoline weight fraction
% y3: gas weight fraction
% y4: gas weight fraction
% convention value: 1 - y1
%
% Author:
%
%     Rui Cai
%% Data
xdata = [1/60, 1/30, 1/20, 1/10]; 
conversion = [0.4926, 0.6204, 0.7118, 0.8238]; 
VGO = [0.5074, 0.3796, 0.2882,0.1762];
gasoline = [0.3767, 0.4385, 0.4865, 0.5416];
gas = [0.0885, 0.136,0.1681 ,0.2108];
coke = [0.0274,0.0459, 0.0572, 0.0714];
ydata = [VGO; gasoline; gas; coke];
%% Guesses for parameters k1, k2, k3
k12guess = 0.1;
k13guess = 0.1;
k14guess = 0.1;
k23guess = 0.1;
k24guess = 0.1;

params_guess = [k12guess, k13guess, k14guess, k23guess, k24guess];
%% Initial conditions for ODEs 
x0(1) = 1;
x0(2) = 0;
x0(3) = 0;
x0(4) = 0;
%% Options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX',1e-14,'TolFun',1e-14,'StepTolerance',1e-14);  
%% Estimate parameters
[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,xdata) ...
    ODEmodel4lump(params,xdata,x0),params_guess,xdata,ydata, [], [], OPTIONS)

xforplotting = linspace(xdata(1),xdata(end),101);
y_calcrefined = ODEmodel4lump(params,xforplotting,x0);

figure(1)
hold on
plot(xdata,ydata(1,:),'rx')
plot(xdata,ydata(2,:),'g+')
plot(xdata,ydata(3,:),'bd')
plot(xdata,ydata(4,:),'y*')
xlabel('time (h)')
ylabel('yield wt. fraction')
plot(xforplotting,y_calcrefined(1,:),'r-')
plot(xforplotting,y_calcrefined(2,:),'g-')
plot(xforplotting,y_calcrefined(3,:),'b-')
plot(xforplotting,y_calcrefined(4,:),'y-')
hold off
legend('VGO','gasoline','gas','coke')

figure(2)
hold on
plot(conversion,ydata(1,:),'rx')
plot(conversion,ydata(2,:),'g+')
plot(conversion,ydata(3,:),'bd')
plot(conversion,ydata(4,:),'y*')
xlabel('conversion wt. fraction')
ylabel('yield wt. fraction')
plot(1-y_calcrefined(1,:),y_calcrefined(1,:),'r-')
plot(1-y_calcrefined(1,:),y_calcrefined(2,:),'g-')
plot(1-y_calcrefined(1,:),y_calcrefined(3,:),'b-')
plot(1-y_calcrefined(1,:),y_calcrefined(4,:),'y-')
hold off
legend('VGO','gasoline','gas','coke')

disp(['k12=', num2str(params(1))])
disp(['k13=', num2str(params(2))])
disp(['k14=', num2str(params(3))])
disp(['k23=', num2str(params(4))])
disp(['k24=', num2str(params(5))])

%% Define ODE
function dydt = ODE4lump(t,y,params)
    k12 = params(1);
    k13 = params(2);
    k14 = params(3);
    k23 = params(4);
    k24 = params(5);
    dydt(1) = -(k12+k13+k14)*(y(1)^2);
    dydt(2) = k12*(y(1)^2)-k23*y(2)-k24*y(2);
    dydt(3) = k13*(y(1)^2)+k23*y(2);
    dydt(4) = k14*(y(1)^2)+k24*y(2);
    dydt = dydt';
end

%% Solve ODE
function y_output = ODEmodel4lump(params,xdata,x0)
    for i = 1:length(xdata);
        tspan = [0:0.001:xdata(i)];
        [~,y_calc] = ode23s(@(t,y) ODE4lump(t,y,params),tspan,x0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end
