%% Problem&Purpose
% 
%The three-lump model involves three subgroups: VGO (y1), gasoline (y2),
%and the sum of gas and coke (y3). The equations that describe the three-lump 
%model are:
%
%    dy1/dt = - (k1 + k3)*y1^2
%    dy2/dt = k1*y1^2 - k2*y2
%    dy3/dt = k3*y1^2 + k2*y2 
%
%   Figure 1 The relationship between time and components fraction
%   Figure 2 The relationship between convension value and components
%    fraction
% 
% The units and definitions of parameters:
%
% Time (h)
% y1: VGO weight fraction
% y2: gasoline weight fraction
% y3: gas and coke weight fraction
% convention value: 1 - y1
%
% Author:
%
%     Rui Cai
%% Data
xdata = [1/60, 1/30, 1/20, 1/10]; 
conversion = [0.4926, 0.6204, 0.7118, 0.8238]; 
VGO = [0.5074, 0.3796, 0.2882,0.1762];
gasoline = [0.3767, 0.4385, 0.4865, 0.5416];
gas = [0.0885, 0.136,0.1681 ,0.2108];
coke = [0.0274,0.0459, 0.0572, 0.0714];
ydata = [VGO; gasoline; gas+coke];
%% Guesses for parameters k1, k2, k3
k1guess = 0.1;
k2guess = 0.1;
k3guess = 0.1;
params_guess = [k1guess, k2guess, k3guess];
%% Initial conditions for ODEs 
x0(1) = 1;
x0(2) = 0;
x0(3) = 0;
%% Options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX',1e-14,'TolFun',1e-14,'StepTolerance',1e-14);  
%% Estimate parameters
[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,xdata) ...
    ODEmodel3lump(params,xdata,x0),params_guess,xdata,ydata, [], [], OPTIONS)

xforplotting = linspace(xdata(1),xdata(end),101);
y_calcrefined = ODEmodel3lump(params,xforplotting,x0);

figure(1)
hold on
plot(xdata,ydata(1,:),'rx')
plot(xdata,ydata(2,:),'g+')
plot(xdata,ydata(3,:),'bd')
xlabel('time (h)')
ylabel('yield wt. fraction')
plot(xforplotting,y_calcrefined(1,:),'r-')
plot(xforplotting,y_calcrefined(2,:),'g-')
plot(xforplotting,y_calcrefined(3,:),'b-')
hold off
legend('VGO','gasoline','gas+coke')

figure(2)
hold on
plot(conversion,ydata(1,:),'rx')
plot(conversion,ydata(2,:),'g+')
plot(conversion,ydata(3,:),'bd')
xlabel('conversion wt. fraction')
ylabel('yield wt. fraction')
plot(1-y_calcrefined(1,:),y_calcrefined(1,:),'r-')
plot(1-y_calcrefined(1,:),y_calcrefined(2,:),'g-')
plot(1-y_calcrefined(1,:),y_calcrefined(3,:),'b-')
hold off
legend('VGO','gasoline','gas+coke')

disp(['k1=', num2str(params(1))])
disp(['k2=', num2str(params(2))])
disp(['k3=', num2str(params(3))])

%% Define ODE
function dydt = ODE3lump(t,y,params)
    k1 = params(1);
    k2 = params(2);
    k3 = params(3);
    dydt(1) = -(k1+k3)*(y(1)^2);
    dydt(2) = k1*(y(1)^2)-k2*y(2);
    dydt(3) = k3*(y(1)^2)+k2*y(2);
    dydt = dydt';
end

%% Solve ODE
function y_output = ODEmodel3lump(params,xdata,x0)
    for i = 1:length(xdata);
        tspan = [0:0.001:xdata(i)];
        [~,y_calc] = ode23s(@(t,y) ODE3lump(t,y,params),tspan,x0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end
