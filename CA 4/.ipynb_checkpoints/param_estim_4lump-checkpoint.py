import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Data
xaxisData = np.array( [1/60, 1/30, 1/20, 1/10] ) # time 
yaxisData = np.array( [ [0.5074, 0.3796, 0.2882,0.1762], [0.3767, 0.4385, 0.4865, 0.5416], [0.0885+0.0274, 0.136+0.0459,0.1681+0.0572,0.2108+0.0714] ] )
conversion = np.array( [0.4926, 0.6204, 0.7118, 0.8238] )
# Guesses for parameters 
k1guess = 0.1; 
k2guess = 0.1; 
k3guess = 0.1; 
parameterGuesses = np.array([k1guess,k2guess,k3guess])

#Define ODEs 
def ODE_definition(y,t,args): 
    y1 = y[0]
    y2 = y[1]
    y3 = y[2]
    k1 = args[0]
    k2 = args[1]
    k3 = args[2]
    dy1dt = -(k1+k3)*y1**2
    dy2dt = k1*y1**2 - k2*y2
    dy3dt = k3*y1**2 + k2*y2
    return dy1dt, dy2dt, dy3dt
 
def ODEmodel(xaxisData,*params):
    yaxis0 = np.array([1,0,0])
    numYaxisVariables = 3 
    yaxisOutput = np.zeros((xaxisData.size,numYaxisVariables)) 
    for i in np.arange(0,len(xaxisData)):
        xaxisIncrement = 0.001 
        xaxisSpan = np.arange(0,xaxisData[i]+xaxisIncrement,xaxisIncrement)
        y_calc = odeint(ODE_definition,yaxis0,xaxisSpan,args=(params,))
        yaxisOutput[i,:]=y_calc[-1,:]
        
    yaxisOutput = np.transpose(yaxisOutput)
    yaxisOutput = np.ravel(yaxisOutput)
    return yaxisOutput

parametersoln, pcov = curve_fit(ODEmodel,xaxisData,np.ravel(yaxisData),p0=parameterGuesses)


plt.figure(1)
plt.plot(xaxisData, yaxisData[0,:],'o') 
plt.plot(xaxisData, yaxisData[1,:],'x') 
plt.plot(xaxisData, yaxisData[2,:],'d')
yaxis0 = np.array([100,1])
numYaxisVariables = 3
xforPlotting = np.linspace(0,xaxisData[-1],100) 
y_calculated = ODEmodel(xforPlotting,*parametersoln)
y_calculated = np.reshape(y_calculated,(numYaxisVariables,xforPlotting.size))
line1 =plt.plot(xforPlotting, y_calculated[0,:],'r-',label='VGO') 
line2 =plt.plot(xforPlotting, y_calculated[1,:],'g-',label='gasoline') 
line3 =plt.plot(xforPlotting, y_calculated[2,:],'b-',label='gas+coke')
plt.xlabel('time (h)')
plt.ylabel('yield wt. fraction')
plt.legend()
plt.show()

plt.figure(2)
plt.plot(conversion, yaxisData[0,:],'o') 
plt.plot(conversion, yaxisData[1,:],'x') 
plt.plot(conversion, yaxisData[2,:],'d')
yaxis0 = np.array([100,1])
numYaxisVariables = 3
xforPlotting = np.linspace(0,xaxisData[-1],100) 
y_calculated = ODEmodel(xforPlotting,*parametersoln)
y_calculated = np.reshape(y_calculated,(numYaxisVariables,xforPlotting.size))
line1 =plt.plot(1-y_calculated[0,:], y_calculated[0,:],'r-',label='VGO') 
line2 =plt.plot(1-y_calculated[0,:], y_calculated[1,:],'g-',label='gasoline') 
line3 =plt.plot(1-y_calculated[0,:], y_calculated[2,:],'b-',label='gas+coke') 
plt.xlabel('conversion wt. fraction')
plt.ylabel('yield wt. fraction')
plt.legend()
plt.show()

print('K1 =',parametersoln[0])
print('K2 =',parametersoln[1])
print('K3 =',parametersoln[2])