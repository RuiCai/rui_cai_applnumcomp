%%
% purpose & output:
%
%    The mass concentration curves of A and B over time
%
% input:
%
%    t: time, h
%    C: matrix([Ca,Cb]) of A and B concentrations, mg/L
%    k1: Rate Constant, /h
%    k2: Rate Constant, /h
%    k3: Rate Constant, mg/(L*h)
%    k4: Rate Constant, /h
%
% differential equations for A and B concentrations over time:
%
% $$\frac{dC_A}{d_t} = -k_1\cdot C_A - k_2\cdot C_A$$
%
% $$\frac{dC_B}{d_t} = k_1\cdot C_A - k_3 - k_4\cdot C_B$$
%
% author:
%
%     Rui Cai
%%
function output = system_of_ODEs(varargin)
% 3 input cases, use default values for absent parameters
    if nargin == 6
        t = varargin{1};
        C = varargin{2};
        k1 = varargin{3};
        k2 = varargin{4};
        k3 = varargin{5};
        k4 = varargin{6};
    elseif nargin == 2
        t = varargin{1};
        C = varargin{2};
        k1 = 0.15;
        k2 = 0.6;
        k3 = 0.1;
        k4 = 0.2;
    else
        t = 0;
        C = [6.25,0];
        k1 = 0.15;
        k2 = 0.6;
        k3 = 0.1;
        k4 = 0.2;
    end

% numerical solution of differential equations
    function dy = fy(x,y)
        dy =zeros(2,1);
        dy(1) = -k1*y(1)-k2*y(1);
        dy(2) = k1*y(1)-k3-k4*y(2);
    end

options=odeset('NonNegative',[1,2]);
[x,y]=ode45(@fy,[t,t+10],C,options);

% plot
plot(x,y)
xlabel('t(h)')
ylabel('C(mg/L)')
xlim([0 inf])
ylim([0 inf])
legend('Ca','Cb','Location','Best');
title('Mass concentrations of A and B')
grid on
end
